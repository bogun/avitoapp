//
//  TVGitHubSearchEngineTests.m
//  Avito
//
//  Created by Valentin on 01.08.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TVGitHubSearchEngine.h"
#import "TVGitHubRequestModel.h"
#pragma GCC diagnostic ignored "-Wundeclared-selector"

@interface TVGitHubSearchEngineTests : XCTestCase  {
    TVGitHubSearchEngine *_searchEngine;
    XCTestExpectation *serverRespondExpectation;
}

@end

@implementation TVGitHubSearchEngineTests

- (void)setUp {
    [super setUp];
    _searchEngine = [[TVGitHubSearchEngine alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


- (void) testInitializationRequestModel {
    XCTAssertNotNil(_searchEngine.requestModel);
}

- (void) testStartWithRequest {
     _searchEngine.requestModel.q = @"Hello world";
    NSURLRequest *request = [_searchEngine.requestModel getRequest];
    XCTestExpectation *expectation = [self expectationWithDescription:@"Get search engine result"];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                            XCTAssertNotNil(data, "Data should not be nil");
                                            XCTAssertNil(error, "You have an error");
                                            if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                                XCTAssertEqual(httpResponse.statusCode, 200, @"HTTP response status code should be 200");
                                                XCTAssertEqualObjects(httpResponse.URL.absoluteString, [request.URL absoluteString], @"HTTP response URL should be equal to original URL");
                                            } else {
                                                  XCTFail(@"Response was not NSHTTPURLResponse");
                                            }
                                            [expectation fulfill];
                                          }];
    [task resume];
    
    [self waitForExpectationsWithTimeout:1.0f handler:^(NSError *error) {
        if (error != nil) {
            NSLog(@"Error: %@", error.localizedDescription);    
        }
        [task cancel];
    }];
}

@end
