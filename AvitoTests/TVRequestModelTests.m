//
//  TVRequestModelTests.m
//  Avito
//
//  Created by Valentin on 01.08.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TVRequestModel.h"
#import <objc/runtime.h>
#import <objc/message.h>
#pragma GCC diagnostic ignored "-Wundeclared-selector"

@interface TVRequestModelTests : XCTestCase {
    TVRequestModel *_requestModel;
}

@end

@implementation TVRequestModelTests

- (void)setUp {
    [super setUp];
    _requestModel = [[TVRequestModel alloc] init];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


- (void) testValidationFields {
    _requestModel.searchURL = @"https://someUrl.com";
    NSError *error = nil;
    void (*objc_msgSendTyped)(id self, SEL _cmd, NSError **error) = (void*)objc_msgSend;
    objc_msgSendTyped(_requestModel, @selector(validationFields:), &error);
    XCTAssertNil(error);
}

- (void) testExsistField {
    NSString *fieldSting = @"field";
    BOOL isExist = [_requestModel performSelector:@selector(isExistField:) withObject:fieldSting];
    XCTAssertTrue(isExist);
}

- (void) testReplaceSpaceToPlus {
    NSString *stringWithSpace = @"Hello my beautiful world";
    NSError *error = NULL;
    NSRegularExpression *regexSpaces = [NSRegularExpression regularExpressionWithPattern:@" " options:NSRegularExpressionCaseInsensitive error:&error];
    NSUInteger numberOfSpaces = [regexSpaces numberOfMatchesInString:stringWithSpace options:0 range:NSMakeRange(0, [stringWithSpace length])];
    NSString *stingWithPlus = [_requestModel performSelector:@selector(replaceSpaceToPlusForString:) withObject:stringWithSpace];
    NSRegularExpression *regexPluses = [NSRegularExpression regularExpressionWithPattern:@"+" options:NSRegularExpressionIgnoreMetacharacters error:&error];
    NSUInteger numberOfPluses = [regexPluses numberOfMatchesInString:stingWithPlus options:0 range:NSMakeRange(0, [stingWithPlus length])];
    XCTAssertGreaterThanOrEqual(numberOfPluses, numberOfSpaces);
}

- (void) testPropertyAsString {
    unsigned int numberOfProperties = 0;
    objc_property_t *propertyArray = class_copyPropertyList([TVRequestModel class], &numberOfProperties);
    objc_property_t property = propertyArray[0];
    id propertyName = [_requestModel performSelector: @selector(propertyAsString:)
                                          withObject:(__bridge id)(property)];
    XCTAssertTrue([propertyName isKindOfClass:[NSString class]]);
    free(propertyArray);
}


@end
