//
//  TVITunesRequestModelTests.m
//  Avito
//
//  Created by Valentin on 01.08.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TVITunesRequestModel.h"
#pragma GCC diagnostic ignored "-Wundeclared-selector"

@interface TVITunesRequestModelTests : XCTestCase {
    TVITunesRequestModel *_requestModel;
}
@end

@implementation TVITunesRequestModelTests

- (void)setUp {
    [super setUp];
    _requestModel = [[TVITunesRequestModel alloc] init];
    _requestModel.searchURL = @"https://someUrl.com";
    _requestModel.term = @"Hello My beautiful world";
    _requestModel.country = @"ru";
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


- (void) testGetRequest {
    NSURLRequest *request = [_requestModel performSelector:@selector(getRequest)];
    XCTAssertNotNil(request);
}

- (void) testPreparedRequest {
    NSURLRequest *request = [_requestModel performSelector:@selector(preparedRequest)];
    XCTAssertNotNil(request);
}

- (void) testGenerateBodyFromPropertiesAndValues {
    NSString *body = [_requestModel performSelector:@selector(generateBodyFromPropertiesAndValues)];
    XCTAssertGreaterThan([body length], 0);
}

- (void) testArrayPropertiesAndValues {
    NSArray *array = [_requestModel performSelector:@selector(arrayPropertiesAndValues)];
    XCTAssertGreaterThan(array.count, 0);
}

@end
