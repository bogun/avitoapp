//
//  TVTableViewCell.m
//  Avito
//
//  Created by Valentin on 01.08.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import "TVTableViewCell.h"
#import "TVTopCellLabel.h"
#import "TVBottomCellLabel.h"

@interface TVTableViewCell () <UIGestureRecognizerDelegate> {
    
}

@end

@implementation TVTableViewCell

#pragma mark - initialization elements

- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initAllElements];
    }
    return self;
}

- (void) initAllElements {
    [self initImageView];
    [self initTopLabel];
    [self initBottomLabel];
}

- (void) initImageView {
    _avatarView = [[UIButton alloc] init];
    _avatarView.layer.cornerRadius = kDefaultImageCornerRadius;
    _avatarView.layer.masksToBounds = YES;
    [self.contentView addSubview:_avatarView];
    _avatarView.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void) initTopLabel {
    _topLabel = [[TVTopCellLabel alloc] init];
    [self.contentView addSubview:_topLabel];
    _topLabel.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void) initBottomLabel {
    _bottomLabel = [[TVBottomCellLabel alloc] init];
    [self.contentView addSubview:_bottomLabel];
    _bottomLabel.translatesAutoresizingMaskIntoConstraints = NO;
}

#pragma mark - set info to cell
- (void) setTopLabelText:(NSString*)topText
         bottomLabelText:(NSString*)bottomText {
    [_topLabel setText:topText];
    [_bottomLabel setText:bottomText];
}

#pragma mark - show cell position
- (void) showITunesEvenCell {
    [self resetAllConstraints];
    [self showLeftCell];
}

- (void) showITunesOddCell {
    [self resetAllConstraints];
    [self showRightCell];
}

- (void) showGitHubEvenCell {
    [self resetAllConstraints];
    [self showRightCell];
}

- (void) showGitHubOddCell {
    [self resetAllConstraints];
    [self showLeftCell];
}

- (void) showLeftCell {
    [self.contentView addSubview:self.avatarView];
    [self showLeftAvatar];
    [self showRightTopLabel];
    [self showRigthBottomLabel];
}

- (void) showRightCell {
    [self.contentView addSubview:self.avatarView];
    [self showRightAvatar];
    [self showLeftTopLabel];
    [self showLeftBottomLabel];
}

#pragma mark - show left cell position

- (void) showLeftAvatar {
    [[self.avatarView tv_pinEdgesToSameEdgesOfSuperView:UIRectEdgeTop|UIRectEdgeBottom|UIRectEdgeLeft] tv_installConstraints];
    [[self.avatarView tv_width:kDefaultCellHeight] tv_install];
}

- (void) showRightTopLabel {
    [[self.topLabel tv_pinEdge:UIRectEdgeLeft toEdge:UIRectEdgeRight ofView:self.avatarView withInset:kDefaultPadding] tv_install];
    [[self.topLabel tv_pinEdgesToSameEdgesOfSuperView:UIRectEdgeTop|UIRectEdgeRight] tv_installConstraints];
    [[self.topLabel tv_height:kDefaultCellHeight/2.0f] tv_install];
}

- (void) showRigthBottomLabel {
    [[self.bottomLabel tv_pinEdge:UIRectEdgeLeft toEdge:UIRectEdgeRight ofView:self.avatarView withInset:kDefaultPadding] tv_install];
    [[self.bottomLabel tv_pinEdge:UIRectEdgeTop toEdge:UIRectEdgeBottom ofView:self.topLabel] tv_install];
    [[self.bottomLabel tv_pinEdgesToSameEdgesOfSuperView:UIRectEdgeBottom|UIRectEdgeRight] tv_installConstraints];
}


#pragma mark - show right position

- (void) showRightAvatar {
    [[self.avatarView tv_pinEdgesToSameEdgesOfSuperView:UIRectEdgeTop|UIRectEdgeBottom|UIRectEdgeRight] tv_installConstraints];
    [[self.avatarView tv_width:kDefaultCellHeight] tv_install];
}

- (void) showLeftTopLabel {
    [[self.topLabel tv_pinEdge:UIRectEdgeRight toEdge:UIRectEdgeLeft ofView:self.avatarView withInset:-kDefaultPadding] tv_install];
    [[self.topLabel tv_pinEdgesToSameEdgesOfSuperView:UIRectEdgeTop|UIRectEdgeLeft] tv_installConstraints];
    [[self.topLabel tv_height:kDefaultCellHeight/2.0f] tv_install];
}

- (void) showLeftBottomLabel {
    [[self.bottomLabel tv_pinEdge:UIRectEdgeRight toEdge:UIRectEdgeLeft ofView:self.avatarView withInset:-kDefaultPadding] tv_install];
    [[self.bottomLabel tv_pinEdge:UIRectEdgeTop toEdge:UIRectEdgeBottom ofView:self.topLabel] tv_install];
    [[self.bottomLabel tv_pinEdgesToSameEdgesOfSuperView:UIRectEdgeLeft| UIRectEdgeBottom] tv_installConstraints];
}


- (void) resetAllConstraints {
    [_avatarView tv_resetConstraints];
    [_topLabel tv_resetConstraints];
    [_bottomLabel tv_resetConstraints];
    [self removeElementsFromCell];
    [self addElementsToCell];
}

- (void) removeElementsFromCell {
    [_avatarView removeFromSuperview];
    [_topLabel removeFromSuperview];
    [_bottomLabel removeFromSuperview];
}

- (void) addElementsToCell {
    [self.contentView addSubview:_avatarView];
    [self.contentView addSubview:_topLabel];
    [self.contentView addSubview:_bottomLabel];
}


@end
