//
//  TVTableViewCell.h
//  Avito
//
//  Created by Valentin on 01.08.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSArray+AutoLayout.h"
#import "NSLayoutConstraint+AutoLayout.h"
#import "UIView+AutoLayout.h"
#import "TVUIConst.h"

@interface TVTableViewCell : UITableViewCell

@property(strong, nonatomic) UIButton *avatarView;
@property(strong, nonatomic) UILabel *topLabel;
@property(strong, nonatomic) UILabel *bottomLabel;

- (void) showITunesEvenCell;
- (void) showITunesOddCell;
- (void) showGitHubEvenCell;
- (void) showGitHubOddCell;
/** Reset all constraints for all elements of cell
 */
- (void) resetAllConstraints;
/** Set text to cell labels
 *  @param topText A string will load to topLabel
 *  @param bottomText A string will load to bottomLabel
 */
- (void) setTopLabelText:(NSString*)topText
         bottomLabelText:(NSString*)bottomText;

@end

