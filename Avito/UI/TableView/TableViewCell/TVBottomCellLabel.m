//
//  TVBottomCellLabel.m
//  Avito
//
//  Created by Valentin on 01.08.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import "TVBottomCellLabel.h"
#import "TVUIConst.h"

@implementation TVBottomCellLabel

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setStyle];
    }
    return self;
}

- (void)setStyle {
    [super setStyle];
    [self setFont:[UIFont fontWithName:kDefaultFont size:kDefaultFontSize]];
}


@end
