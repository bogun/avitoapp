//
// Created by Valentin Titov on 15.02.15.
// Copyright (c) 2015 Browsec. All rights reserved.
//

@import UIKit;

@interface NSLayoutConstraint (AutoLayout)
- (void)tv_installWithPriority:(UILayoutPriority)priority;
- (void)tv_install;
+ (void)tv_installConstraints:(NSArray *)constraints;
- (void)tv_removeConstraint;
+ (void)tv_removeConstraints:(NSArray *)constraints;
@end
