//
// Created by Valentin Titov on 15.02.15.
// Copyright (c) 2015 Browsec. All rights reserved.
//

#import "NSLayoutConstraint+AutoLayout.h"
#import "UIView+AutoLayout.h"

@implementation NSLayoutConstraint (AutoLayout)

- (void)tv_installWithPriority:(UILayoutPriority)priority {
    self.priority = priority;
    if (self.firstItem && !self.secondItem) {
        [self.firstItem addConstraint:self];
    } else if (self.firstItem && self.secondItem) {
        UIView *commonSuperView = [self.firstItem tv_commonSuperViewWithView:self.secondItem];
        [commonSuperView addConstraint:self];
    }
}

- (void)tv_install {
    [self tv_installWithPriority:UILayoutPriorityRequired];
}

+ (void)tv_installConstraints:(NSArray *)constraints {
    for (id object in constraints) {
        if ([object isKindOfClass:[NSLayoutConstraint class]]) {
            [((NSLayoutConstraint *) object) tv_install];
        } else if ([object isKindOfClass:[NSArray class]]) {
            [NSLayoutConstraint tv_installConstraints:object];
        }
    }
}

- (void)tv_removeConstraint {
    if (self.secondItem) {
        UIView *commonSuperview = [self.firstItem tv_commonSuperViewWithView:self.secondItem];
        while (commonSuperview) {
            if ([commonSuperview.constraints containsObject:self]) {
                [commonSuperview removeConstraint:self];
                return;
            }
            commonSuperview = commonSuperview.superview;
        }
    } else {
        [self.firstItem removeConstraint:self];
    }
}

+ (void)tv_removeConstraints:(NSArray *)constraints {
    for (id object in constraints) {
        if ([object isKindOfClass:[NSLayoutConstraint class]]) {
            [((NSLayoutConstraint *) object) tv_removeConstraint];
        } else if ([object isKindOfClass:[NSArray class]]) {
            [NSLayoutConstraint tv_removeConstraints:object];
        }
    }
}

@end
