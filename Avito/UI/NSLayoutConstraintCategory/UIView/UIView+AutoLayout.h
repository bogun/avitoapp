//
// Created by Valentin Titov on 15.02.15.
// Copyright (c) 2015 Anton Dobkin. All rights reserved.
//

#import <UIKit/UIView.h>

UIKIT_STATIC_INLINE UIEdgeInsets ADEdgeInsets(CGFloat inset) {
    return (UIEdgeInsets) {inset, inset, -inset, -inset};
}

typedef NS_OPTIONS(NSInteger, ADAxis) {
    ADAxisY = 1 << 0,
    ADAxisX = 1 << 1,
};

@interface UIView (AutoLayout)

+ (instancetype)tv_viewWithAutoLayout;
- (NSLayoutConstraint *)tv_pinEdge:(UIRectEdge)edge toEdge:(UIRectEdge)toEdge ofView:(UIView *)view withInset:(CGFloat)inset relation:(NSLayoutRelation)relation;
- (NSLayoutConstraint *)tv_pinEdge:(UIRectEdge)edge toEdge:(UIRectEdge)toEdge ofView:(UIView *)view withInset:(CGFloat)inset;
- (NSLayoutConstraint *)tv_pinEdge:(UIRectEdge)edge toEdge:(UIRectEdge)toEdge ofView:(UIView *)view;

- (NSArray *)tv_pinEdgesToSameEdgesOfSuperView:(UIRectEdge)edges withInsets:(UIEdgeInsets)insets;
- (NSArray *)tv_pinEdgesToSameEdgesOfSuperView:(UIRectEdge)edges;

- (NSArray *)tv_pinAllEdgesToSameEdgesOfSuperView:(UIEdgeInsets)insets;
- (NSArray *)tv_pinAllEdgesToSameEdgesOfSuperView;

- (NSLayoutConstraint *)tv_pinEdgeToSameEdgeOfSuperView:(UIRectEdge)edge withInset: (CGFloat) inset relation: (NSLayoutRelation) relation;
- (NSLayoutConstraint *)tv_pinEdgeToSameEdgeOfSuperView:(UIRectEdge)edge withInset: (CGFloat) inset;
- (NSLayoutConstraint *)tv_pinEdgeToSameEdgeOfSuperView:(UIRectEdge)edge;

- (NSLayoutConstraint *)tv_toAlignOnAxis:(ADAxis)axis withInset:(CGFloat)inset;
- (NSLayoutConstraint *)tv_toAlignOnAxisYWithInset:(CGFloat)inset;
- (NSLayoutConstraint *)tv_toAlignOnAxisXWithInset:(CGFloat)inset;
- (NSLayoutConstraint *)tv_toAlignOnAxisY;
- (NSLayoutConstraint *)tv_toAlignOnAxisX;

- (NSLayoutConstraint *)tv_height:(CGFloat)height relation: (NSLayoutRelation)relation;
- (NSLayoutConstraint *)tv_height:(CGFloat)height;
- (NSLayoutConstraint *)tv_width:(CGFloat)width relation: (NSLayoutRelation)relation;
- (NSLayoutConstraint *)tv_width: (CGFloat)width;

- (NSLayoutConstraint *)tv_heightEqualToHeightOfView:(UIView *)view relation:(NSLayoutRelation)relation;
- (NSLayoutConstraint *)tv_heightEqualToHeightOfView:(UIView *)view;

- (NSLayoutConstraint *)tv_widthEqualToWidthOfView:(UIView *)view relation:(NSLayoutRelation)relation;
- (NSLayoutConstraint *)tv_widthEqualToWidthOfView:(UIView *)view;



- (UIView *)tv_commonSuperViewWithView:(UIView *)view;
- (void) tv_resetConstraints;

@end

