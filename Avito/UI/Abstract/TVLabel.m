//
//  TVLabel.m
//  Avito
//
//  Created by Valentin on 01.08.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import "TVLabel.h"
#import "TVUIConst.h"

@implementation TVLabel

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setStyle];
    }
    return self;
}

- (void) setStyle {
    [self setTextColor:kDefaultColor];
}

@end
