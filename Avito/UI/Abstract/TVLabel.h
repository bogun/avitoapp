//
//  TVLabel.h
//  Avito
//
//  Created by Valentin on 01.08.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TVLabel : UILabel

- (void) setStyle;

@end
