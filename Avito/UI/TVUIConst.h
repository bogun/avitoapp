//
//  TVUIConst.h
//  Avito
//
//  Created by Valentin on 01.08.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#ifndef TVUIConst_h
#define TVUIConst_h
#define kDefaultColor [UIColor colorWithRed:70.0f/255.0f green:130.0f/255.0f blue:180.0f/255.0f alpha:1.0f]
static CGFloat const kDefaultCellHeight = 60.0f;
static CGFloat const kDefaultPadding = 5.0f;
static CGFloat const kDefaultCellPadding = 2.0f;
static CGFloat const kDefaultImageCornerRadius = 15.0f;
static CGFloat const kDefaultAnimationDuration = 0.4f;
static NSString* const kHeaderFont = @"HelveticaNeue-Bold";
static NSString* const kDefaultFont = @"HelveticaNeue-Light";
static float const kDefaultFontSize = 16.0f;

#endif /* TVUIConst_h */
