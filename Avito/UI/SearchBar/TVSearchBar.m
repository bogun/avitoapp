//
//  TVSearchBar.m
//  Avito
//
//  Created by Valentin on 01.08.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import "TVSearchBar.h"
#import "TVUIConst.h"

@implementation TVSearchBar

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setStyle];
        [self setDefaultSettings];
        
    }
    return self;
}

- (void) setStyle {
    self.barTintColor = kDefaultColor;
}

- (void) setDefaultSettings {
    self.placeholder = @"Search";
}
@end
