//
//  TVGitHubResultModel.m
//  Avito
//
//  Created by Valentin on 01.08.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import "TVGitHubResultModel.h"

@implementation TVGitHubResultModel

- (instancetype)initWithInfo:(NSDictionary*)info
{
    self = [super init];
    if (self) {
        self.topTitle = info[@"login"];
        self.bottomTitle = info[@"url"];
        self.imageLink = info[@"avatar_url"];
    }
    return self;
}


@end
