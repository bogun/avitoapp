//
//  TVITunesResultModel.m
//  Avito
//
//  Created by Valentin on 01.08.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import "TVITunesResultModel.h"

@implementation TVITunesResultModel

- (instancetype)initWithInfo:(NSDictionary*)info
{
    self = [super init];
    if (self) {
        self.topTitle = info[@"artistName"];
        self.bottomTitle = info[@"trackName"];
        self.imageLink = info[@"artworkUrl100"];
    }
    return self;
}

@end
