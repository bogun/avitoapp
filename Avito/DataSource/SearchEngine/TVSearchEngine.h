//
//  TVSearchEngine.h
//  Avito
//
//  Created by Valentin on 01.08.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol TVSearchEngineDataSource <NSObject>
/** Call when loaded data parsed
 *  @param result An array with parsed info
 */
- (void) didParsedSearchEngineResult:(NSArray*) result;

@end

@interface TVSearchEngine : NSObject

@property(assign, nonatomic) id <TVSearchEngineDataSource> dataSource;

/** Prepare request for search string
 *  @param  searchText A string contain text for search
 */
- (void) search:(NSString*)searchText;
/** Execute request
 *  @param  httpRequest A NSURLRequest instance contain prepared request
 */
- (void)startWithRequest:(NSURLRequest*)httpRequest;
/** Call when data did load from internet
 *  @param  content A dictionary with full info loaded from internet
 */
- (void) didLoadContent:(NSDictionary*) content;

@end
