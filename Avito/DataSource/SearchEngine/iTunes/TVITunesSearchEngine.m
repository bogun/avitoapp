//
//  TVITunesSearchEngine.m
//  Avito
//
//  Created by Valentin on 01.08.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import "TVITunesSearchEngine.h"
#import "TVITunesRequestModel.h"
#import "TVITunesResultModel.h"


@implementation TVITunesSearchEngine


#pragma mark - initialization
- (instancetype)init {
    self = [super init];
    if (self) {
        [self initializationRequestModel];
    }
    return self;
}

- (void) initializationRequestModel {
    _requestModel = [[TVITunesRequestModel alloc] init];
    _requestModel.searchURL = kSearchURL;
    _requestModel.country = @"ru";
}


#pragma mark - Search
- (void)search:(NSString *)searchText {
    _requestModel.term = searchText;
    NSURLRequest *request = [_requestModel getRequest];
    if (request) {
        [self startWithRequest:request];
    } else {
        NSLog(@"Error generation request. Please, try again");
    }
}


#pragma mark - Recieve result
- (void) didLoadContent:(NSDictionary*) content {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSArray *parsedResult = [self parsedResultForContent:content];
        [self.dataSource didParsedSearchEngineResult:parsedResult];
    });
    
}


#pragma mark - Parse
- (NSArray*) parsedResultForContent:(NSDictionary*) content {
    NSMutableArray *parsedResult = [NSMutableArray array];
    NSArray *notParsedResult = content[@"results"];
    for (NSDictionary *info in notParsedResult) {
        TVITunesResultModel *model = [[TVITunesResultModel alloc] initWithInfo:info];
        [parsedResult addObject:model];
    }
    return parsedResult;
}


@end
