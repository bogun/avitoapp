//
//  TVITunesSearchEngine.h
//  Avito
//
//  Created by Valentin on 01.08.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVSearchEngine.h"
@class TVITunesRequestModel;
/* Search engine link */
static NSString* const kSearchURL = @"https://itunes.apple.com/search?";

@interface TVITunesSearchEngine : TVSearchEngine

@property(readonly, nonatomic) TVITunesRequestModel *requestModel;

@end
