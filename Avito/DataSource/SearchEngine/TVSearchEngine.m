//
//  TVSearchEngine.m
//  Avito
//
//  Created by Valentin on 01.08.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import "TVSearchEngine.h"

@interface TVSearchEngine () <NSURLSessionDelegate>

@end


@implementation TVSearchEngine

-(void)search:(NSString *)searchText {
    
}

- (void)startWithRequest:(NSURLRequest*)httpRequest {
    NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig
                                                          delegate:self
                                                     delegateQueue:nil];
    [[session downloadTaskWithRequest:httpRequest] resume];
}


- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    NSError *error = nil;
    NSDictionary * content = [self contentAtLocation:location];
    if (content) {
        [self didLoadContent:content];
        [[NSFileManager defaultManager] removeItemAtURL:location error:&error];
        [session invalidateAndCancel];
        session = nil;
    }
}

- (void) didLoadContent:(NSDictionary*) content {
    NSLog(@"Content did load");
}


- (NSDictionary*) contentAtLocation:(NSURL*) location {
    
    NSString *stringContent = [self stringContentAtLocation:location];
    if (!stringContent) {
        return nil;
    }
    NSError *error;
    NSData *objectData = [stringContent dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dictResponde = [NSJSONSerialization JSONObjectWithData:objectData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&error];
    if (error) {
        NSLog(@"%@", error);
        return nil;
    }
    return dictResponde;
}

- (NSString*) stringContentAtLocation:(NSURL*) location {
    NSString *strLocation = [location absoluteString];
    strLocation = [strLocation substringFromIndex:7];   // Get right path without "file://"
    if (![[NSFileManager defaultManager] fileExistsAtPath:strLocation]) {
        NSLog(@"Error. File with result of request are not exists at location: %@", strLocation);
        return nil;
    }
    NSString* content = [NSString stringWithContentsOfFile:strLocation
                                                  encoding:NSUTF8StringEncoding
                                                     error:NULL];
    content = [content stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return content;
}





@end
