//
//  TVGitHubSearchEngine.h
//  Avito
//
//  Created by Valentin on 01.08.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVSearchEngine.h"
@class TVGitHubRequestModel;
/* Search engine link */
static NSString* const kSearchURL = @"https://api.github.com/search/users?";

@interface TVGitHubSearchEngine : TVSearchEngine

@property(readonly, nonatomic) TVGitHubRequestModel *requestModel;

@end
