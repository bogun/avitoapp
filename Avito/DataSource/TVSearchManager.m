//
//  TVSearchManager.m
//  Avito
//
//  Created by Valentin on 01.08.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import "TVSearchManager.h"
#import "TVSearchEngine.h"
#import <objc/message.h>

@interface TVSearchManager () <TVSearchEngineDataSource>

@end

@implementation TVSearchManager

+ (instancetype) searchManager {
    static TVSearchManager *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[TVSearchManager alloc] init];
    });
    return shared;
}

- (void) searchIn:(NSString*)searchIn
         withText:(NSString*)searchText {
    
    TVSearchEngine *searchEngine = [self initializationClassInstanceForCurrentSearch:searchIn];
    SEL selector = [self selectorForSearch];
    [self searchIn:searchEngine
      withSelector:selector
          withText:searchText];
}

- (id) initializationClassInstanceForCurrentSearch:(NSString*)searchIn {
    NSString *capitalisedName = [searchIn stringByReplacingCharactersInRange:NSMakeRange(0,1)
                                                                  withString:[[searchIn substringToIndex:1] capitalizedString]];
    NSString *strClassName = [NSString stringWithFormat:@"TV%@SearchEngine",capitalisedName];
    TVSearchEngine *searchEngine = [[NSClassFromString(strClassName) alloc] init];
    searchEngine.dataSource = self;
    return searchEngine;
}

- (SEL) selectorForSearch {
    return NSSelectorFromString(@"search:");
}

- (void) searchIn:(id)object
     withSelector:(SEL) selector
         withText:(NSString*) searchText {
    void (*objc_msgSendTyped)(id self, SEL _cmd, id searchText) = (void*)objc_msgSend;
    objc_msgSendTyped(object, selector, searchText);
}


#pragma mark - TVSearchDataSource methods

-(void)didParsedSearchEngineResult:(NSArray *)result {
    [self.dataSource didReceiveSearchResult:result];
}

@end
